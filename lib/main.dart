import 'package:flutter/material.dart';
import 'package:todolist/classes/GridPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Lato',
        brightness: Brightness.light,
        primaryColor: Colors.deepOrange[100],
        accentColor: Colors.deepOrange[200],
      ),
      home: GridPage(title: 'Delivecrous'),
    );
  }
}

