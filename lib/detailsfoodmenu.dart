import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:todolist/PanierList.dart';

class detailsfoodmenu extends StatelessWidget {
  bool monval = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Delivecrous"),
          actions: <Widget>[
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => PanierList()));
              },
              child: Padding(
              padding: const EdgeInsets.only(right:15.0,top: 20.0),
              child:Badge(
                badgeContent: Text("2",
                ),
                child: Icon(Icons.shopping_cart,color: Colors.black),

              ),
            ),
            ),
          ]
      ),
      body: Container(
        child:Column(
          children: <Widget>[
            Image.asset("icons/image1.jpg"),
            Row(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(left:5.0,top: 10),
                  child: Text("Lorem Ipsum dolor",
                    style: TextStyle(fontSize: 20.0),),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left:150.0,top:10),
                  child: Text("10€",
                      style: TextStyle(color: Colors.deepOrange[100])),
                )
              ],
            ),
            Row(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(left:5.0,top:0),
                  child: Text("Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",
                    style: TextStyle(fontSize: 10.0),),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left:205),
                  child: Checkbox(
                    activeColor: Colors.red,
                    value:true,
                    onChanged: (bool value){
                        monval = value;

                    },
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(left:5.0,top:0),
                  child: Text("Allergenes",style: TextStyle(fontSize: 20.0),),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left:237),
                  child: Text(""),
                )
              ],
            ),
            Row(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(left:5.0,top:0),
                  child: Text("- Lorem ipsum",
                    style: TextStyle(fontSize: 10.0),),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left:237),
                  child: Text(""),
                )
              ],
            ),
            Row(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(left:5.0,top:0),
                  child: Text("- Lorem ipsum",
                    style: TextStyle(fontSize: 10.0),),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left:237),
                  child: Text(""),
                )
              ],
            ),

            Row(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(left:5.0,top:0),
                  child: Text("- Lorem ipsum",
                    style: TextStyle(fontSize: 10.0),),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left:237),
                  child: Text(""),
                )
              ],
            ),
          ],
        )
      ),
    );
  }
}